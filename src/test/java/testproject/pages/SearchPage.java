package testproject.pages;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;

import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Selenide.$x;
import static com.codeborne.selenide.Selenide.page;

public class SearchPage {
    public SelenideElement searchField = $x("//input[@title=\"Поиск\"]");

    @Step("Ввести запрос в поисковую строку")
    public ResultsPage goToResultPage(String request) {
        searchField.should(visible).setValue(request).pressEnter();
        return page(ResultsPage.class);
    }

}
