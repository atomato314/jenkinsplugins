package testproject.pages;

import com.codeborne.selenide.Selenide;
import io.qameta.allure.Step;

import static com.codeborne.selenide.Selenide.page;

public class LoginPage {

    @Step("Переход на страницу поиска")
    public SearchPage goToGoogle() {
        Selenide.open("https://www.google.com/");
        return page(SearchPage.class);
    }
}
