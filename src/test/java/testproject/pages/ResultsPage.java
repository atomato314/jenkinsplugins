package testproject.pages;

import com.codeborne.selenide.CollectionCondition;
import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;

import static com.codeborne.selenide.Condition.text;
import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.$$x;
import static com.codeborne.selenide.Selenide.page;

public class ResultsPage {
    public ElementsCollection results = $$x("//div[@class=\"g\"]");

    @Step("Проверить результаты поиска")
    public ResultsPage checkResults(String request) {
        results.filterBy(text(request))
                .shouldHave(CollectionCondition.sizeGreaterThanOrEqual(3));
        return page(ResultsPage.class);
    }
}
