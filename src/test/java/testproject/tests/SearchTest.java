package testproject.tests;

import com.adaptavist.tm4j.junit.annotation.TestCase;
import org.junit.jupiter.api.Test;
import testproject.pages.LoginPage;

public class SearchTest {
    LoginPage loginPage = new LoginPage();

    @Test
    @TestCase(key = "TEST-T1")
    public void test() {
        loginPage.goToGoogle().
                goToResultPage("Jenkins").
                checkResults("Jenkins");
    }
}
